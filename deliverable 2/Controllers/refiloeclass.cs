﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace deliverable_2.Controllers
{
    public class refiloeclass
    {
        //quiz questions for diversity
        private int mQID;
        private string mQuestion;
        private string mAnswer;
        private string mOption1;
        private string mOption2;
        private string mOption3;

        //quiz questions for humah rights
        private int hQID;
        private string hQuestion;
        private string hAnswer;
        private string hOption1;
        private string hOption2;
        private string hOption3;

        //moderate activity dversity
        private int eQID;
        private string eQuestion;
        private string eAnswer;
        private string eOption1;
        private string eOption2;
        private string eOption3;

        //moderate activity human rights
        private int rQID;
        private string rQuestion;
        private string rAnswer;
        private string rOption1;
        private string rOption2;
        private string rOption3;

        //advanced activity diversity
        private int aQID;
        private string aQuestion;
        private string aAnswer;
        private string aOption1;
        private string aOption2;
        private string aOption3;

        //advanced activity human rights
        private int lQID;
        private string lQuestion;
        private string lAnswer;
        private string lOption1;
        private string lOption2;
        private string lOption3;



        public int QID
        {
            get
            {
                return mQID;
            }

            set
            {
                mQID = value;
            }
        }

        public string Qeustion
        {
            get
            {
                return mQuestion;
            }

            set
            {
                mQuestion = value;
            }
        }

        public string Answer
        {
            get
            {
                return mAnswer;
            }

            set
            {
                mAnswer = value;
            }
        }

        public string Option1
        {
            get
            {
                return mOption1;
            }

            set
            {
                mOption1 = value;
            }
        }

        public string Option2
        {
            get
            {
                return mOption2;
            }

            set
            {
                mOption2 = value;
            }
        }

        public string Option3
        {
            get
            {
                return mOption3;
            }

            set
            {
                mOption3 = value;
            }
        }

        public int EQID
        {
            get
            {
                return eQID;
            }

            set
            {
                eQID = value;
            }
        }

        public string EQuestion
        {
            get
            {
                return eQuestion;
            }

            set
            {
                eQuestion = value;
            }
        }

        public string EAnswer
        {
            get
            {
                return eAnswer;
            }

            set
            {
                eAnswer = value;
            }
        }

        public string EOption1
        {
            get
            {
                return eOption1;
            }

            set
            {
                eOption1 = value;
            }
        }

        public string EOption2
        {
            get
            {
                return eOption2;
            }

            set
            {
                eOption2 = value;
            }
        }

        public string EOption3
        {
            get
            {
                return eOption3;
            }

            set
            {
                eOption3 = value;
            }
        }

        public int HQID
        {
            get
            {
                return hQID;
            }

            set
            {
                hQID = value;
            }
        }

        public string HQuestion
        {
            get
            {
                return hQuestion;
            }

            set
            {
                hQuestion = value;
            }
        }

        public string HAnswer
        {
            get
            {
                return hAnswer;
            }

            set
            {
                hAnswer = value;
            }
        }

        public string HOption1
        {
            get
            {
                return hOption1;
            }

            set
            {
                hOption1 = value;
            }
        }

        public string HOption2
        {
            get
            {
                return hOption2;
            }

            set
            {
                hOption2 = value;
            }
        }

        public string HOption3
        {
            get
            {
                return hOption3;
            }

            set
            {
                hOption3 = value;
            }
        }

        public int RQID
        {
            get
            {
                return rQID;
            }

            set
            {
                rQID = value;
            }
        }

        public string RQuestion
        {
            get
            {
                return rQuestion;
            }

            set
            {
                rQuestion = value;
            }
        }

        public string RAnswer
        {
            get
            {
                return rAnswer;
            }

            set
            {
                rAnswer = value;
            }
        }

        public string ROption1
        {
            get
            {
                return rOption1;
            }

            set
            {
                rOption1 = value;
            }
        }

        public string ROption2
        {
            get
            {
                return rOption2;
            }

            set
            {
                rOption2 = value;
            }
        }

        public string ROption3
        {
            get
            {
                return rOption3;
            }

            set
            {
                rOption3 = value;
            }
        }

        public int AQID
        {
            get
            {
                return aQID;
            }

            set
            {
                aQID = value;
            }
        }

        public string AQuestion
        {
            get
            {
                return aQuestion;
            }

            set
            {
                aQuestion = value;
            }
        }

        public string AAnswer
        {
            get
            {
                return aAnswer;
            }

            set
            {
                aAnswer = value;
            }
        }

        public string AOption1
        {
            get
            {
                return aOption1;
            }

            set
            {
                aOption1 = value;
            }
        }

        public string AOption2
        {
            get
            {
                return aOption2;
            }

            set
            {
                aOption2 = value;
            }
        }

        public string AOption3
        {
            get
            {
                return aOption3;
            }

            set
            {
                aOption3 = value;
            }
        }

        public int LQID
        {
            get
            {
                return lQID;
            }

            set
            {
                lQID = value;
            }
        }

        public string LQuestion
        {
            get
            {
                return lQuestion;
            }

            set
            {
                lQuestion = value;
            }
        }

        public string LAnswer
        {
            get
            {
                return lAnswer;
            }

            set
            {
                lAnswer = value;
            }
        }

        public string LOption1
        {
            get
            {
                return lOption1;
            }

            set
            {
                lOption1 = value;
            }
        }

        public string LOption2
        {
            get
            {
                return lOption2;
            }

            set
            {
                lOption2 = value;
            }
        }

        public string LOption3
        {
            get
            {
                return lOption3;
            }

            set
            {
                lOption3 = value;
            }
        }
    }
}