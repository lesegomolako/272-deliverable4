﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace deliverable_2.Controllers
{
    public class Class1
    {
        private int mQID;
        private string mQuestions;
        private string mAnswer;
        private string mOption1;
        private string mOption2;
        private string mOption3;

        public int MQID
        {
            get
            {
                return mQID;
            }

            set
            {
                mQID = value;
            }
        }

        public string MQuestions
        {
            get
            {
                return mQuestions;
            }

            set
            {
                mQuestions = value;
            }
        }

        public string MAnswer
        {
            get
            {
                return mAnswer;
            }

            set
            {
                mAnswer = value;
            }
        }

        public string MOption1
        {
            get
            {
                return mOption1;
            }

            set
            {
                mOption1 = value;
            }
        }

        public string MOption2
        {
            get
            {
                return mOption2;
            }

            set
            {
                mOption2 = value;
            }
        }

        public string MOption3
        {
            get
            {
                return mOption3;
            }

            set
            {
                mOption3 = value;
            }
        }
    }


}