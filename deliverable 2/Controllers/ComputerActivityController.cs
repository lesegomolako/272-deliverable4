﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using deliverable_2.Models;

namespace deliverable_2.Controllers
{
    public class ComputerActivityController : Controller
    {
        // GET: ComputerActivity
        EducationEntities db = new EducationEntities();
        public ActionResult ComputerActivity()
        {
            List<ActivityQuestion> ActivityQuestions = db.ActivityQuestions.Where(zz => zz.Activity_ID == 7).ToList();
            List<ActivityQuestion> ActQuestion = new List<ActivityQuestion>();

            for (int j = 0; j < 20; j++)
            {
                ActQuestion.Add(ActivityQuestions[j]);
            }
            return View(ActQuestion);
        }

        public ActionResult ComputerActivity2()
        {
            List<ActivityQuestion> ActivityQuestions = db.ActivityQuestions.Where(zz => zz.Activity_ID == 7).ToList();
            List<ActivityQuestion> ActQuestion = new List<ActivityQuestion>();

            for (int j = 20; j < 40; j++)
            {
                ActQuestion.Add(ActivityQuestions[j]);
            }
            return View(ActQuestion);
        }

        public ActionResult ComputerQuiz()
        {
            List<QuizQuestion> QuizQuestions = db.QuizQuestions.Where(zz => zz.QQ_ID > 60 && zz.QQ_ID < 81).ToList();
            List<QuizQuestion> QQuestion = new List<QuizQuestion>();

            for (int j = 0; j < 20; j++)
            {
                QQuestion.Add(QuizQuestions[j]);
            }
            return View(QQuestion);
        }
        public ActionResult ComputerScreen()
        {
            return View();
        }
    }
}