﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using deliverable_2.Models;

namespace deliverable_2.Controllers
{
    public class MathPractiseController : Controller
    {
        EducationEntities db = new EducationEntities();
        public ActionResult ActivityOne()
        {
            List<ActivityQuestion> Questions = db.ActivityQuestions.Where(zz => zz.Activity_ID == 2).ToList();

            List<ActivityQuestion> thisQ = new List<ActivityQuestion>();
            List<int> listNumbers = new List<int>();
            int v;
            Random rand = new Random();
            for (int j = 0; j < 10; j++)
            {
                do
                {
                    v = rand.Next(0, 20);
                } while (listNumbers.Contains(v));
                listNumbers.Add(v);
                thisQ.Add(Questions[v]);
            }


            return View(thisQ.ToList());
        }
        public ActionResult Activitytwo()
        {
            List<ActivityQuestion> Questions = db.ActivityQuestions.Where(zz => zz.Activity_ID == 2).ToList();

            List<ActivityQuestion> thisQ = new List<ActivityQuestion>();
            List<int> listNumbers = new List<int>();
            int v;
            Random rand = new Random();
            for (int j = 0; j < 10; j++)
            {
                do
                {
                    v = rand.Next(19, 39);
                } while (listNumbers.Contains(v));
                listNumbers.Add(v);
                thisQ.Add(Questions[v]);
            }


            return View(thisQ.ToList());
        }
        public ActionResult MathQuize()
        {
            List<QuizQuestion> Questions = db.QuizQuestions.Where(zz => zz.QQ_ID > 80 && zz.QQ_ID < 101).ToList();

            List<QuizQuestion> thisQ = new List<QuizQuestion>();
            List<int> listNumbers = new List<int>();
            int v;
            Random rand = new Random();
            for (int j = 0; j < 10; j++)
            {
                do
                {
                    v = rand.Next(0, 20);
                } while (listNumbers.Contains(v));
                listNumbers.Add(v);
                thisQ.Add(Questions[v]);
            }


            return View(thisQ.ToList());
        }
    }
}