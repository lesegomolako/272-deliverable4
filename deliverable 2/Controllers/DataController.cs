﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using deliverable_2.Models;
using System.Security.Cryptography;
using System.Data.SqlClient;        //import SQL Client library

namespace deliverable_2.Controllers
{
    public class DataController : Controller
    {
        SqlConnection myConnect = new SqlConnection("Data Source=.;Initial Catalog=Education;Integrated Security=True");
        // GET: Data
       

        public ActionResult MainMenu()
        {
            return View();
        }
        public ActionResult Settings()
        {
            return View();
        }
        public ActionResult SubjectContent()
        {
            return View();
        }

        public ActionResult Diversity()
        {
            return View();
        }
        public ActionResult ActivityD()
        {
            return View();

        }
        public ActionResult ActivityR()
        {
            return View();
        }

        public ActionResult Rights()
        {
            return View();
        }


        EducationEntities db = new EducationEntities();

        //refiloe was here
        public ActionResult DiversityQuiz(string Question, string Answer, string option1, string option2, string option3)
        {
            //list for class
            List<refiloeclass> DBStuff = new List<refiloeclass>();
            //list for db
            List<QuizQuestion> thisQ = db.QuizQuestions.Where(zz => zz.QQ_ID > 20 && zz.QQ_ID < 41).ToList();

            for (int j = 0; j < 10; j++)
            {
                refiloeclass hqDetails = new refiloeclass();
                hqDetails.QID = thisQ[j].QQ_ID;
                hqDetails.Qeustion = thisQ[j].Question;
                hqDetails.Answer = thisQ[j].QuizAnswer;
                hqDetails.Option1 = thisQ[j].option1;
                hqDetails.Option2 = thisQ[j].option2;
                hqDetails.Option3 = thisQ[j].option3;

                DBStuff.Add(hqDetails);
            }

            return View(DBStuff); //sending the viewmodel to the view(this way, it will send "updated" one each time the Index view loads)
        }

        public ActionResult HRQuiz(string Question, string Answer, string option1, string option2, string option3)
        {
            //list for the class
            List<refiloeclass> DBStuff = new List<refiloeclass>();
            //list for the database
            List<QuizQuestion> thisQ = db.QuizQuestions.Where(zz => zz.QQ_ID > 0 && zz.QQ_ID < 21).ToList();

            for (int j = 0; j < 10; j++)
            {
                refiloeclass details = new refiloeclass();
                details.HQID = thisQ[j].QQ_ID;
                details.HQuestion = thisQ[j].Question;
                details.HAnswer = thisQ[j].QuizAnswer;
                details.HOption1 = thisQ[j].option1;
                details.HOption2 = thisQ[j].option2;
                details.HOption3 = thisQ[j].option3;

                DBStuff.Add(details);
            }
            return View(DBStuff);
        }

        public ActionResult MHumanRights(string Question, string Answer, string option1, string option2, string option3)
        {
            //list for the class
            List<refiloeclass> DBStuff = new List<refiloeclass>();
            //list for the database
            List<ActivityQuestion> thisQ = db.ActivityQuestions.Where(zz => zz.AQ_ID > 0 && zz.AQ_ID < 21).ToList();

            for (int j = 0; j < 10; j++)
            {
                refiloeclass details = new refiloeclass();
                details.RQID = thisQ[j].AQ_ID;
                details.RQuestion = thisQ[j].Question;
                details.RAnswer = thisQ[j].ActAnswer;
                details.ROption1 = thisQ[j].option1;
                details.ROption2 = thisQ[j].option2;
                details.ROption3 = thisQ[j].option3;

                DBStuff.Add(details);
            }
            return View(DBStuff);


        }
        public ActionResult MDiversityActivity(string Question, string Answer, string option1, string option2, string option3)
        {
            //list for the class
            List<refiloeclass> DBStuff = new List<refiloeclass>();
            //list for the database
            List<ActivityQuestion> thisQ = db.ActivityQuestions.Where(zz => zz.AQ_ID > 20 && zz.AQ_ID < 41).ToList();

            for (int j = 0; j < 10; j++)
            {
                refiloeclass Details = new refiloeclass();
                Details.EQID = thisQ[j].AQ_ID;
                Details.EQuestion = thisQ[j].Question;
                Details.EAnswer = thisQ[j].ActAnswer;
                Details.EOption1 = thisQ[j].option1;
                Details.EOption2 = thisQ[j].option2;
                Details.EOption3 = thisQ[j].option2;

                DBStuff.Add(Details);
            }
            return View(DBStuff);
        }

        public ActionResult ADiversityActivity(string Question, string Answer, string option1, string option2, string option3)
        {
            //list for the class
            List<refiloeclass> DBStuff = new List<refiloeclass>();
            //list for the database
            List<ActivityQuestion> thisQ = db.ActivityQuestions.Where(zz => zz.AQ_ID > 20 && zz.AQ_ID < 41).ToList();

            for (int j = 0; j < 10; j++)
            {
                refiloeclass Detail = new refiloeclass();
                Detail.AQID = thisQ[j].AQ_ID;
                Detail.AQuestion = thisQ[j].Question;
                Detail.AAnswer = thisQ[j].ActAnswer;
                Detail.AOption1 = thisQ[j].option1;
                Detail.AOption2 = thisQ[j].option2;
                Detail.AOption3 = thisQ[j].option3;

                DBStuff.Add(Detail);
            }
            return View(DBStuff);
        }
        public ActionResult AHumanRights(string Question, string Answer, string option1, string option2, string option3)
        {
            //list for the class
            List<refiloeclass> DBStuff = new List<refiloeclass>();
            //list for the database
            List<ActivityQuestion> thisQ = db.ActivityQuestions.Where(zz => zz.AQ_ID > 40 && zz.AQ_ID < 61).ToList();

            for (int j = 0; j < 10; j++)
            {
                refiloeclass details = new refiloeclass();
                details.LQID = thisQ[j].AQ_ID;
                details.LQuestion = thisQ[j].Question;
                details.LAnswer = thisQ[j].ActAnswer;
                details.LOption1 = thisQ[j].option1;
                details.LOption1 = thisQ[j].option2;
                details.LOption3 = thisQ[j].option3;

                DBStuff.Add(details);
            }
            return View(DBStuff);
        }

        public ActionResult Mathactivity()
        {
            return View();
        }
        public ActionResult Financeactivity()
        {
            return View();
        }

        public ActionResult Math()
        {
            return View();
        }

        public ActionResult Quiz()
        {
            return View();
        }

        public ActionResult Activity()
        {
            return View();
        }

        public ActionResult Finances()
        {
            return View();
        }

        public ActionResult EmailEtiquette()
        {
            return View();
        }

        public ActionResult ComputerSkills()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}