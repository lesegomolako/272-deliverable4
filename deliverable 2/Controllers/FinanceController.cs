﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using deliverable_2.Models;

namespace deliverable_2.Controllers
{
    public class FinanceController : Controller
    {
        // GET: Finance
        EducationEntities db = new EducationEntities();
        public ActionResult FiananceActivityone()
        {
            List<ActivityQuestion> Questions = db.ActivityQuestions.Where(zz => zz.Activity_ID == 3).ToList();

            List<ActivityQuestion> thisQ = new List<ActivityQuestion>();
            List<int> listNumbers = new List<int>();
            int v;
            Random rand = new Random();
            for (int j = 0; j < 10; j++)
            {
                do
                {
                    v = rand.Next(0, 20);
                } while (listNumbers.Contains(v));
                listNumbers.Add(v);
                thisQ.Add(Questions[v]);
            }


            return View(thisQ.ToList());
        }
        public ActionResult FiananceActivitytwo()
        {
            List<ActivityQuestion> Questions = db.ActivityQuestions.Where(zz => zz.Activity_ID == 2).ToList();

            List<ActivityQuestion> thisQ = new List<ActivityQuestion>();
            List<int> listNumbers = new List<int>();
            int v;
            Random rand = new Random();
            for (int j = 0; j < 10; j++)
            {
                do
                {
                    v = rand.Next(19, 39);
                } while (listNumbers.Contains(v));
                listNumbers.Add(v);
                thisQ.Add(Questions[v]);
            }


            return View(thisQ.ToList());
        }
        public ActionResult FianaceQuize()
        {

            List<QuizQuestion> Questions = db.QuizQuestions.Where(zz => zz.QQ_ID > 100).ToList();

            List<QuizQuestion> thisQ = new List<QuizQuestion>();
            List<int> listNumbers = new List<int>();
            int v;
            Random rand = new Random();
            for (int j = 0; j < 10; j++)
            {
                do
                {
                    v = rand.Next(0, 20);
                } while (listNumbers.Contains(v));
                listNumbers.Add(v);
                thisQ.Add(Questions[v]);
            }


            return View(thisQ.ToList());
        }
    }
}