﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using deliverable_2.Models;

namespace deliverable_2.Controllers
{
    public class EmailActivityController : Controller
    {
        // GET: EmailActivity
        EducationEntities db = new EducationEntities();
        public ActionResult EmailActivity()
        {
            List<ActivityQuestion> ActivityQuestions = db.ActivityQuestions.Where(zz => zz.Activity_ID == 6).ToList();
            List<ActivityQuestion> ActQuestion = new List<ActivityQuestion>();

            for (int j = 0; j < 20; j++)
            {
                ActQuestion.Add(ActivityQuestions[j]);
            }
            return View(ActQuestion);


        }
        public ActionResult EmailActivity2()
        {
            List<ActivityQuestion> ActivityQuestions = db.ActivityQuestions.Where(zz => zz.Activity_ID == 6).ToList();
            List<ActivityQuestion> ActQuestion = new List<ActivityQuestion>();

            for (int j = 20; j < 40; j++)
            {
                ActQuestion.Add(ActivityQuestions[j]);
            }
            return View(ActQuestion);


        }
        public ActionResult EmailQuiz()
        {
            List<QuizQuestion> QuizQuestions = db.QuizQuestions.Where(zz => zz.QQ_ID > 40 && zz.QQ_ID < 61).ToList();
            List<QuizQuestion> QQuestion = new List<QuizQuestion>();

            for (int j = 0; j < 20; j++)
            {
                QQuestion.Add(QuizQuestions[j]);
            }
            return View(QQuestion);


        }
        public ActionResult EmailScreen()
        {
            return View();
        }
    }
}