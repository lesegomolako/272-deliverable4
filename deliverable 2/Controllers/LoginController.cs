﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using deliverable_2.Models;
using deliverable_2.ViewModel;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;

namespace deliverable_2.Controllers
{
    public class LoginController : Controller
    {
        EducationEntities db = new EducationEntities();
        UserVM myUser = new UserVM();
        // GET: Login
        [HttpGet]
        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogIn(string username, string password)
        {
            if (username != "" || password != "")
            {
                try
                {
                    User ThisUser = db.Users.Where(zz => zz.Username == username).FirstOrDefault();



                    int user = Convert.ToInt16(ThisUser.User_ID);
                    myUser.SetUser(user);



                    string Pass = ComputePass(password);

                    if (ThisUser.Password == Pass)
                    {
                        return RedirectToAction("MainMenu", "Data");
                    }
                    else
                    {
                        TempData["Empty"] = "Password entered was incorrect! Please re-enter details";
                        return RedirectToAction("Login", "Login");
                    }

                }
                catch
                {
                    TempData["Empty"] = "User details incorrect or invalid. <br/ >Please re-enter details or click 'Sign Up' to create account";
                    return RedirectToAction("Login", "Login");
                }





            }
            else
            {
                TempData["Empty"] = "Enter Username or Password to log in";
                return RedirectToAction("Login", "Login");
            }

        }

        [HttpGet]
        public ActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignUp(string Username, string Password, string Email, string Cellphone)
        {
            myUser.Username = Username;
            myUser.Password = Password;
            myUser.EncryptedPassword = ComputePass(Password);

            if ((Email == null && Cellphone == null) || (Email == "" && Cellphone == ""))
            {
                TempData["Error"] = "You have to enter either your cellphone or email";
                return View();
            }

            if (Email != null || Email != "")
            {
                myUser.Email = Email;
            }

            if (Cellphone != null || Cellphone != "")
            {
                myUser.Cellphone = Cellphone;
            }

            return RedirectToAction("PersonalInfo", "Login");
        }

        [HttpGet]
        public ActionResult PersonalInfo()
        {
            ViewBag.Suburb = new SelectList(db.Suburbs, "Suburb_ID", "Suburb1");
            ViewBag.City = new SelectList(db.Cities, "City_ID", "City1");
            ViewBag.Province = new SelectList(db.Provinces, "Province_ID", "Province1");
            ViewBag.Country = new SelectList(db.Suburbs, "Country_ID", "Country1");
            return View();
        }

        [HttpPost]
        public ActionResult PersonalInfo(string Name, string Surname, string Address1, string Suburb1, string City1, string Province1, string Country1)
        {
            string Username = myUser.Username;
            string Password = myUser.Password;
            string Email = myUser.Email;

            User CreateUser = new User(); //creates the user object
            CreateUser.Username = Username;
            CreateUser.Password = myUser.EncryptedPassword;
            CreateUser.UserRole = 2;
            db.Users.Add(CreateUser);
            db.SaveChanges();


            User ThisUser = db.Users.Where(zz => zz.Username == Username).FirstOrDefault();
            int ID = ThisUser.User_ID;

            Address CreateAddress = new Address(); //saves address of student
            CreateAddress.Address1 = Address1;
            CreateAddress.Suburb_ID = 1;
            db.Addresses.Add(CreateAddress);
            db.SaveChanges();

            Student CreateStudent = new Student(); //creates the student object
            CreateStudent.Name = Name;
            CreateStudent.Surname = Surname;
            CreateStudent.Email = Email;
            CreateStudent.Cellphone = myUser.Cellphone;
            CreateStudent.User_ID = ID;
            CreateStudent.Address_ID = 1;




            db.Students.Add(CreateStudent);
            db.SaveChanges();

            SendMail(Username, Password, Email);
            return RedirectToAction("Login", "Login");
        }


        public void SendMail(string Username, string Password, string Email)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress("lsgmlk1@mail.com", "Lesego");
                mail.To.Add(Email);
                mail.Subject = "User Successfully created";
                mail.Body = "Your User name is: " + Username + "\n" + "Your Password is: " + Password;

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential(mail.From.Address, "concentration");
                SmtpServer.EnableSsl = true;
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;

                SmtpServer.Send(mail);


            }
            catch (Exception err)
            {
                ViewBag.Error = "Some error";
            }
        }


        string ComputePass(string RawData)
        {
            using (SHA256 sha256hash = SHA256.Create())
            {
                byte[] bytes = sha256hash.ComputeHash(Encoding.UTF8.GetBytes(RawData));

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public ActionResult Account()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            int ID = myUser.GetUser();
            User ThisUser = db.Users.Where(zz => zz.User_ID == ID).FirstOrDefault();

            return View(ThisUser);
        }

        [HttpPost]
        public ActionResult ChangePassword(string NewPassword)
        {
            int ID = myUser.GetUser();
            User ThisUser = db.Users.Where(zz => zz.User_ID == ID).FirstOrDefault();
            ThisUser.Password = ComputePass(NewPassword);
            db.Users.Add(ThisUser);
            db.SaveChanges();

            return RedirectToAction("Account", "Login");
        }


        [HttpGet]
        public ActionResult DeleteAccount()
        {

            return View();
        }

        [HttpPost]
        public ActionResult DeleteAccount(string yes)
        {
            int ID = myUser.GetUser();
            User ThisUser = db.Users.Where(zz => zz.User_ID == ID).FirstOrDefault();
            db.Users.Remove(ThisUser);
            return RedirectToAction("Login","Login");
        }

        [HttpGet]
        public ActionResult UpdatePersInfo()
        {
            int ID = myUser.GetUser();
            Student ThisUser = db.Students.Where(zz => zz.User_ID == ID).FirstOrDefault();

            return View(ThisUser);
        }

        [HttpPost]
        public ActionResult UpdatePersInfo(string Name, string Surname, string email)
        {
            int ID = myUser.GetUser();
            Student ThisUser = db.Students.Where(zz => zz.User_ID == ID).FirstOrDefault();
            ThisUser.Name = Name;
            ThisUser.Surname = Surname;
            ThisUser.Email = email;
            db.Students.Add(ThisUser);
            db.SaveChanges();


            return RedirectToAction("Account", "Login");
        }

        public User CurrentLoggedIn()
        {
            int ID = 2;  /*myUser.GetUser();*/ //add code to determine current logged in user
            User CurrentlyLoggedIn = db.Users.Where(zz => zz.User_ID == ID).FirstOrDefault();

            return CurrentlyLoggedIn;
        }

    }
}