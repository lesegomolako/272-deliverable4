﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace deliverable_2.ViewModel
{
    public class UserVM
    {
       

            static private string mUsername;
            static private string mPassword;
            static private string mEncryptedPassword;
            static private string mEmail;
            static private string mCellphone;
            static private int mID;




            public UserVM() { }



            public string Username
            {
                get
                {
                    return mUsername;
                }

                set
                {
                    mUsername = value;
                }
            }

            public string Password
            {
                get
                {
                    return mPassword;
                }

                set
                {
                    mPassword = value;
                }
            }

            public string EncryptedPassword
            {
                get
                {
                    return mEncryptedPassword;
                }

                set
                {
                    mEncryptedPassword = value;
                }
            }

            public string Email
            {
                get
                {
                    return mEmail;
                }

                set
                {
                    mEmail = value;
                }
            }

            public string Cellphone
            {
                get
                {
                    return mCellphone;
                }

                set
                {
                    mCellphone = value;
                }
            }

            public static int MID
            {
                get
                {
                    return mID;
                }

                set
                {
                    mID = value;
                }
            }

            public void SetUser(int UserID)
            {
                mID = UserID;
            }

            public int GetUser()
        {
            return mID;
        }
        }
    }
